import { Component, OnInit } from '@angular/core';
import { CityService } from '../service/city.service';
import { City } from '../model/city.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-city-detail',
  templateUrl: './city-detail.component.html',
  styleUrls: ['./city-detail.component.scss']
})
export class CityDetailComponent implements OnInit {
  
  id?: number;
  city?: City;

  constructor(private cityService: CityService,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    
    this.id = this.route.snapshot.params['id'];

    this.cityService.getCityById(this.id).subscribe(data => {
      this.city = data
    })
  }

}

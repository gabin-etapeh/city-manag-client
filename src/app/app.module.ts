import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VilleInfoComponent } from './ville-info/ville-info.component';
import { VilleListeComponent } from './ville-liste/ville-liste.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CityDetailComponent } from './city-detail/city-detail.component';
@NgModule({
  declarations: [
    AppComponent,
    VilleInfoComponent,
    VilleListeComponent,
    CityDetailComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }

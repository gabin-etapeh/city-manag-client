import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CityService } from '../service/city.service';
import { City } from '../model/city.model';
@Component({
  selector: 'app-ville-liste',
  templateUrl: './ville-liste.component.html',
  styleUrls: ['./ville-liste.component.scss'],
  providers: [CityService]
})
export class VilleListeComponent implements OnInit {

  cities: City[] = [];

  constructor(private cityService: CityService) { }
  
  deleteVille(id: any) {
    this.cityService.deleteCity(id)
      .subscribe(() => {
        console.log('la suppression a reussi')
        this.ngOnInit()
      })
  }

  ngOnInit() {
    this.cityService.getVilleList().subscribe(data => {
      this.cities = data
    });
  }
}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VilleListeComponent } from './ville-liste.component';

describe('VilleListeComponent', () => {
  let component: VilleListeComponent;
  let fixture: ComponentFixture<VilleListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VilleListeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VilleListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

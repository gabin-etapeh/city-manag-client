export class City {
    id?: string;
    nomVille: string;
    description: string;
    dateCreation: string;
    population: number;
    superficie: number;


    constructor(nomVille: string, description: string, dateCreation: string, population: number, superficie: number) {
        this.nomVille = nomVille;
        this.description = description;
        this.population = population;
        this.superficie = superficie;
        this.dateCreation = dateCreation;
    }

}
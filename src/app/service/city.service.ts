import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { City } from "../model/city.model";
import { Observable } from "rxjs";
@Injectable({
   providedIn: 'root'
})
export class CityService {
   constructor(private http: HttpClient) { }

   private baseUrl = 'http://localhost:8080';

   public getVilleList(): Observable<City[]> {
      return <Observable<City[]>>this.http.get(`${this.baseUrl}/villes`)
   }

   createVilleInfo(city: City): Observable<City> {
      return <Observable<City>>this.http.post(`${this.baseUrl}/ville`, city);
   }
   deleteCity(id: any): Observable<any> {
      return <Observable<any>>this.http.delete(`${this.baseUrl}/ville/${id}`)
   }
   updateCity(id?: number, city?: City): Observable<City> {
      return <Observable<City>>this.http.put(`${this.baseUrl}/ville/${id}`, city);
   }
   getCityById(id?: number): Observable<City> {
      return <Observable<City>>this.http.get(`${this.baseUrl}/ville/${id}`);
   }
}

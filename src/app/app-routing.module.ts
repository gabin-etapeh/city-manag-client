import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VilleInfoComponent } from './ville-info/ville-info.component';
import { VilleListeComponent } from './ville-liste/ville-liste.component';
import { CityDetailComponent } from './city-detail/city-detail.component';

const routes: Routes = [
  {
    path: 'add-city', component: VilleInfoComponent
  }, {
    path: 'cities', component: VilleListeComponent
  }, {
    path: 'update-city/:id', component: VilleInfoComponent
  }, {
    path: 'city-detail/:id', component: CityDetailComponent
  },{
    path: '', redirectTo: '/cities', pathMatch: 'full'
  }, {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { CityService } from '../service/city.service';
import { City } from '../model/city.model';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-ville-info',
  templateUrl: './ville-info.component.html',
  styleUrls: ['./ville-info.component.scss'],
})


export class VilleInfoComponent implements OnInit {

  // 1. Bloc des variables
  cityForm: FormGroup = new FormGroup(
    {
      nomVille: new FormControl(''),
      superficie: new FormControl(''),
      population: new FormControl(''),
      description: new FormControl(''),
      dateCreation: new FormControl(''),
    }
  );
  loading = false;
  submitted = false;
  id?: number;
  title = "Add city";

  // 2. Bloc constructeur
  constructor(
    private formBuilder: FormBuilder,
    private cityService: CityService,
    private route: ActivatedRoute,
    private router: Router) { }


  // 3. Ce qui se passe à l'initialisation du composant Angular (cycle de vie d'un composant)
  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.cityForm = this.formBuilder.group(
      {
        nomVille: ['',
          [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
          ],
        ],
        description: ['',
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(40),
          ],
        ],
        dateCreation: ['',
          [
            Validators.required
          ],
        ],
        population: ['', Validators.required],

        superficie: ['', []]
      },
    );

    if (this.id) {
      this.title = 'Update city';
      this.loading = true;
      this.cityService.getCityById(this.id)
        .subscribe(x => {
          this.cityForm.patchValue(x);
          this.loading = false;
        });
    }
  }

  // 4. Méthodes utilitaires dans le composant (déclenchés par appel)

  get f() {
    return this.cityForm.controls;
  }

  onSubmit() {

    this.submitted = true;

    if (this.cityForm.invalid) {
      return;
    }

    let cityName = this.f["nomVille"].value;
    let cityDescription = this.f["description"].value;
    let cityCreationDate = this.f["dateCreation"].value;
    let cityPopulation = this.f["population"].value;
    let cityArea = this.f["superficie"].value;

    let city = new City(cityName, cityDescription, cityCreationDate, cityPopulation, cityArea);
    console.log(city);
    if (this.id) {
      this.cityService.updateCity(this.id, city).subscribe(data => {
        console.log(`City updated successfully: ${data}`)
        this.router.navigateByUrl("/cities")
      })
    }
    else {
      this.cityService.createVilleInfo(city)
        .subscribe(response => {
          console.log(response);
          this.router.navigateByUrl(`/cities`);
        });
    }
  }
  onReset(): void {
    this.submitted = false;
    this.cityForm.reset();
  }

}

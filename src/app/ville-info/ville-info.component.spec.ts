import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VilleInfoComponent } from './ville-info.component';

describe('VilleInfoComponent', () => {
  let component: VilleInfoComponent;
  let fixture: ComponentFixture<VilleInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VilleInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VilleInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
